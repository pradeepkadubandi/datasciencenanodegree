from pandas import *
from datetime import *
from ggplot import *
import scipy
import scipy.stats
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

twd = pandas.read_csv(r'C:\DataScienceNanoDegree\1.IntroToDataScience\turnstile_data_master_with_weather.csv')
twd['UnitNumber'] = twd.UNIT.map(lambda x:int(x[1:]))
twd['Date'] = pandas.to_datetime(twd.DATEn)
twd['DayOfWeek'] = twd.Date.map(lambda x:x.weekday())
twd['IsWeekend'] = twd.DayOfWeek.map(lambda x: x > 4)
twd['AdjustedHour'] = twd.Hour.map(lambda x: x % 4)



twd_improved = pandas.read_csv(r'C:\DataScienceNanoDegree\1.IntroToDataScience\turnstile_weather_v2.csv')

twd_for_hour_on_each_day_for_all_units = twd.pivot_table(index=['Hour','DATEn'], columns='UNIT', values='ENTRIESn_hourly')
twd_for_hour_for_each_unit_on_all_days = twd.pivot_table(index=['Hour','UNIT'], columns='DATEn', values='ENTRIESn_hourly')
twd_for_unit_at_each_hour_on_all_days = twd.pivot_table(index=['UNIT', 'Hour'], columns='DATEn', values='ENTRIESn_hourly')


------------

# Note that order matters :-), No Rain is more than Rain, so if Rain is plotted first it is hidden by No Rain hist :-)
plt.figure()

NoRainAxesPlot = twd_improved['ENTRIESn_hourly'][twd_improved['rain'] == 0].hist(bins=30, range=(0, 21000), color='blue')
RainAxesPlot = twd_improved['ENTRIESn_hourly'][twd_improved['rain'] == 1].hist(bins=30, range=(0, 21000), color='red')

rain_patch = mpatches.Patch(color='red', label='Rain')
no_rain_patch = mpatches.Patch(color='blue', label='No Rain')
plt.legend(handles = [rain_patch, no_rain_patch])


plt.xlabel('Range (ENTRIESn_hourly)')

plt.ylabel(r'frequency (ENTRIESn_hourly)')
plt.title(r'Histogram of ENTRIESn_hourly')

plt.show()
---------------------

#col_name = 'day_week' #Uncomment this to explore ridership by day of week
#col_name = 'hour' #Uncomment this to explore ridership by time of day
col_name = 'UNIT' #Uncomment this to explore ridership by unit, note that only bar_plot is applicable
    
group_by_col = twd_improved.groupby(col_name, as_index=False)
sum_by_col = group_by_col.sum()
sum_by_col.index.name = col_name
base_plot = ggplot(sum_by_col, aes(col_name, 'ENTRIESn_hourly'))
line_plot = base_plot + geom_line(stat='identity')
bar_plot = base_plot + geom_bar(stat='identity')
scatter_plot = base_plot + geom_point()
line_plot_with_smooth_curve = line_plot + stat_smooth(colour="red")
bar_plot_with_smooth_curve = bar_plot + stat_smooth(colour="red")
scatter_plot_with_smooth_curve = scatter_plot + stat_smooth(colour="red")
